//两种调试信息输出：1、alert函数2、console(客户端浏览器)控制台输出

//数值类型变量
var age=23;
alert(age);

//字符类型变量
var question="中国人\'中国地";
alert(question);

//布尔类型变量
var is_true=true;
console.log(is_true);

//数组类型变量
var animals=Array();
animals[0]="狗";
animals[1]="猫";
console.log(animals);

//关联数组
var product=Array();
product["name"]="笔记本电脑";
product["price"]=4500.0;
console.log(product["price"]);

//对象
var student= new Object();
student.name="张三";
student.age=19;
console.log(student);

//选择结构
var number=5;
if(number>=5&&number>=10){
    console.log("输入数据符合要求!");
}

//while 循环
var count =1;
while(count<6){
    console.log(count);
    count++;
}

//for循环
for(var i=0;i<6;i++){
    console.log(i);
}

//函数
function square(num){
    return num*num;
}
console.log(square(20));

//匿名函数
var x=function (a,b) {
    return a+b;
}

console.log(x(3,2));
//匿名函数的自执行
console.log("匿名函数自执行形势一："+(function(a,b){return a*b;})(3,4));
console.log("匿名函数自执行形势二："+(function(a,b){return a-b;}(10,2)));

//对象方法
var person = {
    name:"李四",age:20
};
//增加方法
person.show=function () {
    console.log("姓名："+ name +",年龄："+age);
}
person.show();
//访问对象采用for..in结构
for(var attr in person){
    console.log(typeof  person[attr]);
}
//常见对象使用 jQuery
var person2={
    name:"王五",age:21,
    success:function () {
        console.log("成功");
    }
};
person2.success();