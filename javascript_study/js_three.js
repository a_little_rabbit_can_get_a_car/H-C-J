//通过元素id访问
var objById=document.getElementById("province");
console.log(typeof objById);
console.log(objById);

//通过标签访问
var objByTagName=document.getElementsByTagName("li");
console.log(typeof  objByTagName);
for(var i=0;i<objByTagName.length;i++){
    console.log(objByTagName[i]);
}

//通过类访问
var objByClassName=document.getElementsByClassName("cur");
console.log(typeof objByClassName);
for(var i=0;i<objByClassName.length;i++){
    console.log(objByClassName[i]);
}

//遍历元素节点
function traversal(node) {
    //node为元素节点（nodeType为1）
    if(node&&node.nodeType===1){
        console.log(node.nodeName);
    }
    //子节点
    var childNodes=node.childNodes;
    for(var i=0;i<childNodes.length;i++){
        var item=childNodes[i];
        if(item.nodeType===1){
            traversal(item);
        }
    }
}
traversal(document.documentElement);
//属性节点访问
var objp=document.getElementsByTagName("p");
for(var i=0;i<objp.length;i++){
    var txtTitle=objp[i].getAttribute("title");
}

document.getElementById("province").setAttribute("title","省份列表");