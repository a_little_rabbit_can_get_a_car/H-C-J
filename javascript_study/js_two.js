//访问网页DOM,采用id访问，由于ID的唯一性，获得的是对象
function $(id) {
    return document.getElementById(id);
}
//单击事件
function click_me() {
    console.log("点击事件");
    //访问html对象的属性
    $("btn_test").innerHTML="已经点击！";
}

function register_check() {
    var username=$("u_name").value;
    var password=$("u_pwd").value;
    var passwordConfirm=$("u_pwd2").value;
    if(username===''||password===''){
        alert("用户名和密码不能为空");
        return false;
    }
    else{
        //判断用户名、密码等字符串的合法性通常使用“正则表达式”
        if(username.length<6){
            alert("用户名至少6个字符");
        }
            else if(password.length<6){
                alert("密码至少6个字符");
                return false;
        }
        else if(password!=passwordConfirm){
            alert("两次输入密码必须一致");
            return false;
        }
        }
        return true;
}

function clear_info() {
    var flag=confirm("确认要清除数据吗？");
    if(true===flag){
        $("username").value='';
        $("password").value='';
        $("passwordConfirm").value='';
    }
}