//json数据源
//http://api.douban.com/v2/movie/top250
//格式化工具
//http://tool.oschina.net/codeformat/json

var xmlhttp;

if(window.XMLHttpRequest){
    xmlhttp=new XMLHttpRequest();

}else {
    xmlhttp=new ActiveXObject("microsoft.XMLHTTP");
}

var news_content=document.getElementById("news_content");

//http无状态，不知开始、结束   定义一些状态值来描述http的性质
//定义回调函数
xmlhttp.onreadystatechange = function () {
    if(xmlhttp.readyState===4&& xmlhttp.status===200){
        // news_content.innerHTML=xmlhttp.responseText;
        //json.parse解析为json对象
        showjsondata(JSON.parse(xmlhttp.responseText));
    }
};

//HTTp访问方法get post
xmlhttp.open("GET","tsconfig.json",true);
xmlhttp.send();
//解析json文本
function showjsondata(jsonData) {
    document.getElementById("title").innerText=jsonData.title;
    var tempHTML= ' ';
    for(var i=0; i<10; i++){
        // tempHTML+='<li>'+jsonData.subjects[i].title+"<li>";
        tempHTML+="<li>";
        tempHTML+=jsonData.subjects[i].title;
        tempHTML+="<li>";
    }
    document.getElementById("news_list").innerHTML=tempHTML;
}