# H-C-J

## 开发环境

   * [WebStrom] (http://www.jetbrains.com/webstorm/) 跨平台Web开发工具
   * [visual studio code] (https://code.visualstudio.com/) 跨平台
   * [Git] (https://git-scm.com/)版本控制
   * Chrome / FireFox / IE(Edge) / safri
   m
   * 码云（git.oschna.net）
       * Pages服务
   * GitHub 世界共享代码仓库
   
## HTML基础

   * 默认索引页 &emsp;`index/default.(html/htm/php/jsp/aspx)`
   * 标签语言 共性 
       * <> 包裹 浏览器中不显示 语义
       * 单标签 `<input>`
       * 成对标签 `<a></a>`
   * 文档类型 （建议）`<!DOCTYOE>`
       * html 4.01 严格/兼容
       * xhtml 1.0  严格/兼容
       * xhtml 1.1
       * html 5
   * 树形结构
   * html
   * head
       * title 显示在标题栏上的内容
       * meta 元素数据描述
       * link 链接CSS
   * body
       * h1 h2 h3 h4 h5 h6 标题
       * em/i strong/b 强调
       * p/ pre 段落
       * br /hr 无序列表
       * dl dt dd 定义列表
       * a 超链接 `href target` 页面锚点（自学）
       * image 图片 `src alt`
       * iframe 内嵌框架帧
   * Web语义化
 - - -
        
## CSS基础

   * 前导
       * 每个html标签具有自己的相关属性来描述
       * 每个html标签具有浏览器给予的默认属性值（默认风格）
       * 每个html标签在不同的浏览器中很大可能存在风格差异
   * CSS调试工具
       * 浏览器（chrom/Firefox/Edge/IE11)  F12
       * DOM树选取
       * 风格查看 &emsp;&emsp;`盒子模型`
   * CSS使用方法 &emsp;&emsp;`优先级;style 属性>style 标签>link
       * 元素内 style属性
       * html页面内  style标签
       * CSS文件 link标签  (都这样用)
   * 基本语法
       >selector{ property:value;}
       >
       >注释  ` /*多行或单行*/`
   * 选择器(selector)
       * 元素选择器 element_name(元素名称，即HTLIL标签)
       * ID选择器  #id_name(ID属性：每个页面中唯一存在)
       * 类选择器  .class_name(类属性：可以重复使用)
       * 属性选择器 [属性=值]
       * 通用选择器 * （简单粗暴，非常有效）
       * 组合选择器
           * 相邻兄弟选择器 A+B
           * 普通兄弟选择器 A~B
           * 子选择器 A>B
           * 后代选择器 A B
       * 伪类选择器
   * CSS选择器声明
       * 集体声明 &emsp;&emsp;`h1,h,h3,h4,h5,h6{...}`
       * 全局声明 &emsp;&emsp;`*`
       * 派生声明 &emsp;&emsp;`#sidebar span a`
   * CSS继承与层叠
       * 子标签继承父标签的样式，可修改
       * 层叠优先级 &emsp;&emsp`行内样式>id样式>class样式>元素样式`
   * 区块元素 &emsp;&emsp; `div`
       * 无语义 流容器
       * 常用属性
           * 显示 &emsp; `display`
           * 定位 &emsp; `positon`
           * 边框 &emsp; `border`
           * 尺寸 &emsp; `width /height /min-xxx`
           * 边距 &emsp; `padding/margin/padding-xxx/margin-xxx`
           * 浮动 &emsp; `float`
           * 清除浮动 &emsp; `clear`
           * 溢出控制 &emsp; `overflow`
       * 行内标签 span
       * CSS样式属性
       属性值中的[尺寸单位]
           * em &emsp;&emsp; `相对单位；相对body字体的front-size(16px) 大小单位`
           * rem &emsp;&emsp; `相对单位：相对body字体的front-size(16px)大小单位`
           * % &emsp;&emsp;`百分比`
           * px &emsp;&emsp; `绝对单位  与显示设备相关`
       * 字体样式 &emsp;&emsp; `font(font-style font-weight font-variant font-size/line-height font-family)`
       * 文本样式
           * letter-spacing 字符间距
           * line-height 行高
           * text-indent 文本缩进
           * text-decoration 文本水平对齐
           * vertical-align 垂直对齐
       * 颜色 &emsp;&emsp; `color`
       * 背景 &emsp;&emsp; `backgound(backgound-color background-image background)`
       * 列表 &emsp;&emsp;
       * 盒子模型 M(argin)B(order)P(adding)C(ontent)模型
             * 在content-sizing模式下实际宽度为 M+B+P+C   
             * 边界 `margin(-top/right/bottom/left)`
             * 填充 `padding(-top/right/bottom/left)`
         * 页面布局  div+css
             * CSS Reset [normalize.css](https://necolas.github.io/normalize.css/8.0.0/normalize.css)
             * 定宽模式 栅格系统(Grid System)
                  * [960 Grid System]
             * 宽度自适应模式
                  * 左右结构(左定宽,右自适应)
                  * 左中右结构(左、右定宽,中自适应)
             * 响应式设计
                  * html 5
                  * Viewport 视口 针对硬件
                  * media query 媒介查询
                  * 流式布局
                  * 弹性图片
         * 页面菜单 `ul li + css`
                        
   ---
            
   ## HTML数据呈现——表格
            
   * HTML标签
      * table
        * caption 标题
        * thead 表头(分类)
        * tbody 表内容(分类)
        * tfoot 表(脚)(分类)
        * tr 普通行
        * th 表头行
        * td 单元格
      * 属性
         * colspan 列合并
         * rowspan 行合并
   * 表格的CSS属性
      * border-collapse
      * border-spacing
      * caption-side
      * empty-cells
      * table-layout
                        
## HTML 数据交互——表单
            
   * form `重要属性:action name method(post get)`
       * fieldset &emsp;&emsp; 分组
       * legend &emsp;&emsp; 分组说明
       * input &emsp;&emsp; `重要属性: name type`
       * textarea &emsp;&emsp; 多行文本域(绑定富文本编辑器)
       * select option &emsp;&emsp; 下拉
                    
## 综合示例1
   * 重构 [九江学院信息科学与技术学院]  (http://xxxy.jju.edu.cn/)
       * Chrome + F12
       * Chrome插件 Page
   * [怪兽模块] (https://www.templatemonster.com/cn/)
                                    
## Javascript基础 