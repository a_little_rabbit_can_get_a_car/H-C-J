//图片滚动速度
var speed = 200;

//获取需要处理的DOM元素
var scroll_begin = document.getElementById("scroll_begin");
var scroll_end = document.getElementById("scroll_end");
var scroll_box = document.getElementById("scroll_box");

console.log(scroll_begin);
console.log(scroll_end);
console.log(scroll_box);

//复制图片元素
scroll_end.innerHTML = scroll_begin.innerHTML;
//设置滚动区域
scroll_box.scrollLeft = 0;

// console.log(scroll_begin.offsetWidth);
// console.log(scroll_end.offsetWidth);

function marquee() {
    // console.log(scroll_box.scrollLeft);
    if (scroll_box.scrollLeft >= scroll_end.offsetWidth) {
        scroll_box.scrollLeft -= scroll_begin.offsetWidth;
    } else {
        scroll_box.scrollLeft++;
    }
}

//定时滚动
var flag = setInterval(marquee, speed);

//鼠标事件
+scroll_box.addEventListener(
    "mouseover",
    function () {
        clearInterval(flag);
    }
);

scroll_box.addEventListener(
    "mouseout",
    function () {
        flag = setInterval(marquee, speed);
    }
);