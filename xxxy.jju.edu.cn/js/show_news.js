var xmlhttp;
if(window.XMLHttpRequest){
    //IE7+,FireFox,Chrome,Opera,Safari
    xmlhttp = new XMLHttpRequest();
}else {
    //IE6
    xmlhttp = new ActiveX("Microsoft.XMLHTTP");
}

//HTTP是无状态，定义一些状态值描述当前HTTP的性质
//404(not found) 200(ok) 50x(服务器应用程序错误)
//定义回调函数
xmlhttp.onreadystatechange = function () {
    if(xmlhttp.readyState === 4 && xmlhttp.status === 200){
        //news_content.innerHTML = xmlhttp.responseText;
        //JSON.parse()把文本数据解析为Json对象
        showJson(JSON.parse(xmlhttp.responseText));
    }
}

//HTTP访问方法：GET  POST    ...
//xmlhttp.open("GET","http://api.douban.com/v2/movie/top250",true);
xmlhttp.open("GET","data/news.json",true);
xmlhttp.send();

function showJson(jsonData) {
    var tempHtml = "";
    for(var i = 0; i < 10; i++){
        tempHtml += "<li>";
        tempHtml += "<a href=\""+jsonData.list[i].news_url+"\">"+jsonData.list[i].title+"</a>";
        tempHtml += "<span class=\"time\">"+jsonData.list[i].publish_time+"</span>";
        tempHtml += "</li>\n"
    }
    document.getElementById("news_update_list").innerHTML = tempHtml;
}